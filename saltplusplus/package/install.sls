# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import saltplusplus with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

saltplusplus-package-install-python-mako-pkg-installed:
  pkg.installed:
    - name: {{ saltplusplus.mako.pkg.name }}
