# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import saltplusplus with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set keytype = salt['pillar.get']('saltplusplus:ssh:keytype') %}

saltplusplus-config-ssh-key-managed:
  file.managed:
    - name: /root/.ssh/id_{{ keytype }}
    - contents_pillar: saltplusplus:ssh:key
    - makedirs: True
    - user: root
    - group: root
    - mode: 600

saltplusplus-config-ssh-pub-key-managed:
  file.managed:
    - name: /root/.ssh/id_{{ keytype }}.pub
    - contents_pillar: saltplusplus:ssh:pubkey
    - makedirs: True
    - user: root
    - group: root
    - mode: 600

{# Figure this out using `ssh-keyscan gitlab.com | ssh-keygen -lf -` for fingerprints #}
{# or `ssh-keyscan gitlab.com` for keys #}
saltplusplus-config-ssh-known-hosts-github-key-managed:
  ssh_known_hosts.present:
    - name: github.com
    - user: root
    - key: AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl
    - enc: ssh-ed25519

saltplusplus-config-ssh-known-hosts-gitlab-key-managed:
  ssh_known_hosts.present:
    - name: gitlab.com
    - user: root
    - key: AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf
    - enc: ssh-ed25519

saltplusplus-config-ssh-known-hosts-personal-gitlab-key-managed:
  ssh_known_hosts.present:
    - name: code.ginstoo.net
    - user: root
    - key: AAAAC3NzaC1lZDI1NTE5AAAAIEDrBU9TgSAg3/CzFfHGJ/Hzd2mri1IObB2Ytzmj7ix1
    - enc: ssh-ed25519

saltplusplus-config-ssh-file-root-managed:
  file.directory:
    - name: /srv/salt/ssh/files
    - user: root
    - group: root
    - mode: 700
    - makedirs: True

saltplusplus-config-ssh-key-available-in-fileserver:
  file.symlink:
    - name: /srv/salt/ssh/files/deploy_key
    - target: /root/.ssh/id_{{ keytype }}
    - user: root
    - group: root
    - mode: 600
    - require:
      - saltplusplus-config-ssh-key-managed
      - saltplusplus-config-ssh-file-root-managed
