# frozen_string_literal: true

control 'saltplusplus.package.install' do
  title 'The required package should be installed'

  # Overide by `platform_finger`
  package_name = 'python3-mako'

  describe package(package_name) do
    it { should be_installed }
  end
end
