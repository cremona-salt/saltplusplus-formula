# frozen_string_literal: true

control 'saltplusplus ssh key' do
  title 'should match desired lines'

  describe file('/root/.ssh/id_ed25519') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should include 'BEGIN OPENSSH PRIVATE KEY' }
  end
end

control 'saltplusplus ssh public key' do
  title 'should match desired lines'

  describe file('/root/.ssh/id_ed25519.pub') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should include 'ssh-ed25519 abc' }
  end
end

gitlab_key = 'AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTj'\
             'quxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf'
github_key = 'AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzr'\
             'm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl'
ginstoo_key = 'AAAAC3NzaC1lZDI1NTE5AAAAIEDrBU9TgS'\
              'Ag3/CzFfHGJ/Hzd2mri1IObB2Ytzmj7ix1'

control 'saltplusplus ssh known hosts' do
  title 'should match desired lines'

  describe file('/root/.ssh/known_hosts') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should include "ssh-ed25519 #{gitlab_key}" }
    its('content') { should include "ssh-ed25519 #{github_key}" }
    its('content') { should include "ssh-ed25519 #{ginstoo_key}" }
  end
end

control 'saltplusplus ssh file root directory' do
  title 'should exist'

  describe directory('/srv/salt/ssh/files') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0700' }
  end
end

control 'saltplusplus ssh key symlink' do
  title 'should exist'

  describe file('/srv/salt/ssh/files/deploy_key') do
    it { should be_symlink }
    it { should_not be_directory }
    its('link_path') { should eq '/root/.ssh/id_ed25519' }
  end
end
