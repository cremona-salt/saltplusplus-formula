# frozen_string_literal: true

control 'saltplusplus ssf file root directory' do
  title 'should exist'

  describe directory('/srv/salt/ssf/files/fishers/git') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0700' }
  end
end

control 'saltplusplus ssf git 10 script' do
  title 'should match desired lines'

  describe file('/srv/salt/ssf/files/fishers/git/git_10_prepare.sh') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should include 'Only use this for debugging!' }
    its('content') { should include '# Perform actions depending on' }
    its('content') { should include 'git checkout "${BRANCH_UPSTREAM}"' }
  end
end

control 'saltplusplus ssf git 20 script' do
  title 'should match desired lines'

  describe file('/srv/salt/ssf/files/fishers/git/git_20_commit_push.sh') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should include 'git commit ${AMEND} "${COMMIT_OPTIONS}"' }
    its('content') { should include 'if ${PUSH_ACTIVE}; then' }
    its('content') { should include 'git checkout "${BRANCH_UPSTREAM}"' }
  end
end

control 'saltplusplus ssf git 30 script' do
  title 'should match desired lines'

  describe file('/srv/salt/ssf/files/fishers/git/git_30_create_PR.sh') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should include 'Only use this for debugging!' }
    its('content') { should include 'GH_TOKEN=ABC123' }
    its('content') { should include 'GL_TOKEN=DEF456' }
  end
end

control 'saltplusplus ssf repo directory' do
  title 'should exist'

  describe directory('/srv/repos') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0700' }
  end
end

control 'saltplusplus ssf repo' do
  title 'should be cloned'

  describe directory('/srv/repos/template-formula') do
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
  end
end

control 'saltplusplus ssf repo pillar example file' do
  title 'should match desired lines'

  describe file('/srv/repos/template-formula/pillar.example') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('content') { should include('TEMPLATE:') }
    its('content') { should include('TEMPLATE-config-file-file-managed:') }
  end
end
