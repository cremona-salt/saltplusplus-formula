# frozen_string_literal: true

control 'saltplusplus.package.clean' do
  title 'The required package should be removed'

  # Overide by `platform_finger`
  package_name = 'python3-mako'

  describe package(package_name) do
    it { should_not be_installed }
  end
end
