# frozen_string_literal: true

gitlab_key = 'AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTj'\
             'quxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf'
github_key = 'AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzr'\
             'm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl'
ginstoo_key = 'AAAAC3NzaC1lZDI1NTE5AAAAIEDrBU9TgS'\
              'Ag3/CzFfHGJ/Hzd2mri1IObB2Ytzmj7ix1'

control 'saltplusplus ssh known hosts' do
  title 'should not match desired lines'

  describe file('/root/.ssh/known_hosts') do
    its('content') { should_not include "ssh-ed25519 #{gitlab_key}" }
    its('content') { should_not include "ssh-ed25519 #{github_key}" }
    its('content') { should_not include "ssh-ed25519 #{ginstoo_key}" }
  end
end

control 'saltplusplus ssh key symlink' do
  title 'should not exist'

  describe file('/srv/salt/ssh/files/deploy_key') do
    it { should_not exist }
  end
end

control 'saltplusplus ssh file root directory' do
  title 'should not exist'

  describe directory('/srv/salt/ssh/files') do
    it { should_not exist }
  end
end

control 'saltplusplus ssh key' do
  title 'should not exist'

  describe file('/root/.ssh/id_ed25519') do
    it { should_not exist }
  end
end

control 'saltplusplus ssh public key' do
  title 'should not exist'

  describe file('/root/.ssh/id_ed25519.pub') do
    it { should_not exist }
  end
end
